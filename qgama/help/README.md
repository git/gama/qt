# Qgama help

Since qgama-2.09 the program help is defined in qgama/help/qgama.md in
Markdown format.

## Suggested viewer

https://pandoc.org/

   pandoc -s qgama.md -o view-qgama.html
